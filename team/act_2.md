<!-- START bsis_2_visaya_christian -->

# Aspiring Software Engineer <!-- the one you want to be -->

## Christian Visaya

👋 Aspiring Software Engineer — 💌 christianvisaya@laverdad.edu.ph — Apalit, Pampanga

![alt bsis_2_visaya_christian.jpg](images/bsis_2_visaya_christian.jpg)

### Bio

**Good to know:** I'm a fan of optimization. One thing for sure I've learned throughout my career is that communication and understanding is the most significant part of solving a problem. I prefer clarity over speed.

**Motto:** Stability under pressure

**Languages:** Python, Javascript, PHP

**Other Technologies:** AWS, GCP, Microsoft Azure, Digital Ocean, Alibaba Cloud

**Personality Type:** [Protagonist (ENFJ-A)](https://www.16personalities.com/profiles/c5ea44cd4a89a)

_Know your personality type, traits, role, and strategy at [16Personalities.com](https://www.16personalities.com/)_

<!-- END bsis_2_visaya_christian -->

<!-- START act_2_bigtas_kurtd_daniel -->

# Aspiring Cybersecurity Professional <!-- the one you want to be -->

## Kurtd Daniel Bigtas

👋 Aspiring Cybersecurity Professional — 💌 kurtddanielbigtas@student.laverdad.edu.ph — Apalit, Pampanga

![alt act_2_bigtas_kurtd_daniel.jpg](images/act_2_bigtas_kurtd_daniel.jpg)

### Bio

**Good to know:** I'm a very patient person. I can wait if there's a need. Willing to wait. I hate goodbyes.

**Motto:** In order to be safe, you need to know where you're vulnerable.

**Languages:** Java, C#, Python, CSS, HTML

**Other Technologies:** Kali Linux

**Personality Type:** [Protagonist (ENFJ-T)](https://www.16personalities.com/profiles/c28f51c5fdaac)

_Know your personality type, traits, role, and strategy at [16Personalities.com](https://www.16personalities.com/)_

<!-- END act_2_bigtas_kurtd_daniel -->
